package aufgabe_1_1;

/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 *
 * @author Gruppe 3
 * @version 0.99
 */

public class Aufgabe_1_1_Test {

    public static void toStringTest() {

        Permutation p = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6);

        System.out.println("\nStarte toStringTest()");

        System.out
                .print("Ausgabe einer Permutation mit den Werten 1,2,3,4,5,6: ");
        System.out.println(p);
    }

    public static void toCycleStringTest() {
        Permutation p;

        System.out.println("\nStarte toCycleStringTest()");

        System.out
                .print("Ausgabe einer Permutation mit den Werten 1,2,3,4,5,6: ");
        p = PermutationImplementation.createPermutation(6, 2, 1, 4, 3, 5);
        System.out.println(p.toCycleString());
        System.out.println(p.cycle(0));

        System.out.print("Ausgabe einer Permutation mit den Werten 1: ");
        p = PermutationImplementation.createPermutation(1);
        System.out.println(p.toCycleString());

        System.out.print("Ausgabe einer Permutation mit den Werten 2,1,3: ");
        p = PermutationImplementation.createPermutation(2, 1, 3);
        System.out.println(p.toCycleString());

        System.out.print("Ausgabe einer Permutation mit den Werten 3,2,1: ");
        p = PermutationImplementation.createPermutation(3, 2, 1);
        System.out.println(p.toCycleString());
    }

    public static void getCycleTest() {
        try{
            Permutation p = PermutationImplementation.createPermutation(1, 3, 2, 5, 4, 6);

            System.out.println("\nStarte getCycleTest()");

            System.out.print("Zeige Cycle 1 von (1,3,2,5,4,6): ");

            System.out.println(p.cycle(0));
            System.out.print("Zeige Cycle 2 von (1,3,2,5,4,6): ");
            System.out.println(p.cycle(1));

            System.out.print("Zeige Cycle 3 von (1,3,2,5,4,6): ");
            System.out.println(p.cycle(2));

            System.out.print("Zeige Cycle 4 von (1,3,2,5,4,6): ");
            System.out.println(p.cycle(3));

            System.out.print("Zeige Cycle 5 von (1,3,2,5,4,6): ");
            System.out.println(p.cycle(4));

        }
        catch(IndexOutOfBoundsException e)
        {
            System.out.print("Keine weiteren cyclen vorhanden!!!");
        }
    }

    public static void equalsTest() {
        Permutation p1 = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6);
        Permutation p2 = PermutationImplementation.createPermutation(1, 2, 3, 4, 5);
        String s = "test";

        System.out.println("\nStarte equalsTest()");

        System.out.print("Vergleich mit String: ");
        System.out.println(p1.equals(s));

        System.out
                .print("Vergleich mit Permutation unterschiedlicher Laenge: ");
        System.out.println(p1.equals(p2));

        System.out.print("Vergleich mit Permutation gleicher Laenge "
                + "und unterschiedlichem Inhalt: ");
        p2 = PermutationImplementation.createPermutation(1, 2, 3, 4, 6, 5);
        System.out.println(p1.equals(p2));

        System.out.print("Vergleich mit Permutation gleicher Laenge "
                + "und gleichem Inhalt: ");
        p2 = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6);
        System.out.println(p1.equals(p2));
    }

    public static void getInverseTest() {
        Permutation p = PermutationImplementation.createPermutation(2, 4, 5, 1, 3);

        System.out.println("\nStarte getInverseTest()");

        System.out.print("Zeige inverse Permutation von (2,4,5,1,3): ");
        System.out.println(p.inverse());

        System.out.print("Zeige davon inverse Permutation: ");
        System.out.println(p.inverse().inverse());
    }

    public static void getCompositionTest() {
        Permutation p1 = PermutationImplementation.createPermutation(2, 4, 5, 1, 3);
        Permutation p2 = PermutationImplementation.createPermutation(3, 5, 1, 4, 2);

        System.out.println("\nStarte getCompositionTest()");

        System.out.print("Zeige Kompostion von (2,4,5,1,3) mit (3,5,1,4,2): ");
        System.out.println(p1.compose(p2));

        System.out
                .print("Zeige Kompostion von (2,4,5,1,3) mit Identitaet (1,2,3,4,5): ");
        p2 = PermutationImplementation.createPermutation(1, 2, 3, 4, 5);
        System.out.println(p1.compose(p2));

        System.out.print("Zeige Komposition von (2,4,5,1,3) mit Inverse: ");
        System.out.println(p1.compose(p1.inverse()));

        System.out
                .print("Zeige Kompostion von (2,4,5,1,3) mit Permutation ungleicher Laenge (3,5,1,4,2,6): ");
        p2 = PermutationImplementation.createPermutation(3, 5, 1, 4, 2, 6);
        System.out.println(p1.compose(p2));
    }

    public static void testForAssociativity() {
        Permutation p1 = PermutationImplementation.createPermutation(1, 3, 2);
        Permutation p2 = PermutationImplementation.createPermutation(2, 3, 1);
        Permutation p3 = PermutationImplementation.createPermutation(3, 2, 1);
        Permutation pTmp;

        System.out.println("\nStarte testForAssociativity()");

        System.out.print("Komponiere ((3,2,1) o (2,3,1)) o (1,3,2): ");
        pTmp = p2.compose(p3);
        System.out.println(p1.compose(pTmp));

        System.out.print("Komponiere (3,2,1) o ((2,3,1) o (1,3,2)): ");
        pTmp = p1.compose(p2);
        System.out.println(pTmp.compose(p3));
    }

    public static void checkForMultipleNumberTest() {
        Permutation p;

        System.out.println("\nStarte testCheckForMultipleNumber()");

        System.out
                .print("Erstelle Permutation ohne doppelte Werte (1,2,3,4,5,6): ");
        p = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6);
        System.out.println(p);

        System.out.print("Erstelle kurze Permutation (1): ");
        p = PermutationImplementation.createPermutation(1);
        System.out.println(p);

        System.out
                .print("Erstelle lange Permutation (1,2,3,4,5,6,7,8,9,10,11): ");
        p = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
        System.out.println(p);

        System.out
                .print("Erstelle Permutation mit einem doppelten Wert (1,2,3,4,5,6,7,8,9,10,1): ");
        p = PermutationImplementation.createPermutation(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1);
        System.out.println(p);
    }

    public static void calculateFacultyTest() {
        System.out.println("\nStarte testCalculateFaculty()");

        System.out.print("Berechne Fakultaet von 4: ");
        System.out.println(Util.calculateFaculty(4));
    }

    public static void PermutationGroupConstruktorTest() {
        PermutationGroup pgrp;

        System.out.println("\nPermutationGroupConstruktorTest()");

        System.out.print("Erstelle PermutationGroup, Laenge 1: ");
        pgrp = PermutationGroupImplementation.createGroup(1);
        System.out.println(pgrp);

        System.out.print("Erstelle PermutationGroup, Laenge 3: ");
        pgrp = PermutationGroupImplementation.createGroup(3);
        System.out.println(pgrp);

        System.out.print("Erstelle PermutationGroup, Laenge -1: ");
        pgrp = PermutationGroupImplementation.createGroup(-1);
        System.out.println(pgrp);
    }

    public static void getFixpointsTest() {
        Permutation p;

        System.out.println("\ntestGetFixpoints()");

        System.out.print("Zeige Fixpunkte von Permutation (1): ");
        p = PermutationImplementation.createPermutation(1);
        System.out.println(p.fixpoints());

        System.out.print("Zeige Fixpunkte von Permutation (1,2,3,4): ");
        p = PermutationImplementation.createPermutation(1, 2, 3, 4);
        System.out.println(p.fixpoints());

        System.out.print("Zeige Fixpunkte von Permutation (3,5,1,4,2): ");
        p = PermutationImplementation.createPermutation(3, 2, 1, 4, 5);
        System.out.println(p.fixpoints());
    }

    public static void getOrderTest() {
        Permutation p;

        System.out.println("Tests fuer Order :::");

        p = PermutationImplementation.createPermutation(2,1,3,4,6,7,10,8,9,5);

        System.out.println(p.toCycleString());
        System.out.println(p.cycleType());
        System.out.println(p.cycle(0) + ", " + p.cycle(5));
        System.out.println(p.order());
        System.out.println(p.pow(3));
        System.out.println(p.pow(10000000));
        System.out.println(p.pow(-100000));
    }

    public static void aufgabe1_2 (){
        //Hauptvariablen
        Permutation p1 = PermutationImplementation.createPermutation(3,4,5,1,2);
        PermutationGroup S5 = PermutationGroupImplementation.createGroup(5);
        PermutationGroup S10 = PermutationGroupImplementation.createGroup(10);
        Permutation p2 = S5.createPermutation(2,4,5,1,3);
        Permutation p3 = S10.createPermutation("(1 2)(5 6 7 10)");


        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("Tests fuer Aufgabe 1.2 :::");
        System.out.println();

        System.out.println("         "+p1+","+p2+","+p3);
        System.out.println("Erwartet:(3,4,5,1,2),(2,4,5,1,3),(2,1,3,4,6,7,10,8,9,5)");
        System.out.println();

        System.out.println("         "+p1.applyValue(5) + "," + p2.applyValue(4));
        System.out.println("Erwartet:2,1");
        System.out.println();
        System.out.println("         "+p1.toCycleString() + "," + p2.toCycleString() + "," + p3.toCycleString());
        System.out.println("Erwartet:(1 3 5 2 4),(1 2 4)(3 5),(1 2)(3)(4)(5 6 7 10)(8)(9)");
        System.out.println();
        System.out.println("         "+p1.equals(S5.createPermutation("(1 3 5 2 4)"))+","+p1.equals(p2));
        System.out.println("Erwartet:true,false");
        System.out.println();
        System.out.println("         "+p3.cycle(0)+","+p3.cycle(5));
        System.out.println("Erwartet:(1 2),(9)");
        System.out.println();
        System.out.println("         "+p3.order());
        System.out.println("Erwartet:4");
        System.out.println();
        //Seitenumbruch Seite 4
        System.out.println("         "+p3.cycleType());
        System.out.println("Erwartet:[1⁴ 2 4]");
        System.out.println();

    	Permutation p4 = S10.createPermutation(p3.cycle(0).toString());
    	System.out.println("         "+p4);
    	System.out.println("Erwartet:(2,1,3,4,5,6,7,8,9,10)");
    	System.out.println();

        System.out.println("         "+p3.fixpoints());
        System.out.println("Erwartet:(3 4 8 9)");
        System.out.println();

        System.out.println("         "+p2.compose(p1)+","+p1.compose(p2)+","+(p1.compose(p2).equals(p2.compose(p1))));
        System.out.println("Erwartet:(4,1,2,3,5),(5,1,3,2,4),false");
        System.out.println();

    	//kein zwischenschritt id verfÃ¼gbar
    	System.out.println("         "+S5.createPermutation(1,2,3,4,5).compose(p2.compose(p1)));
    	System.out.println("Erwartet:(4,1,2,3,5)");
    	System.out.println();

        System.out.println("         "+p1.inverse()+","+p1.compose(p1.inverse())+","+p1.inverse().compose(p1));
        System.out.println("Erwartet:(4,5,1,2,3),(1,2,3,4,5),(1,2,3,4,5)");
        System.out.println();

        System.out.println("         "+PermutationImplementation.createPermutation(1, 5, 4, 2, 1) + "," + S5.createPermutation(1, 3, 2, 4, 6, 5));
        System.out.println("Erwartet:NaP,NaP");
        System.out.println();

        System.out.println("         "+p3.compose(p2.compose(p1)));
        System.out.println("Erwartet:NaP");
        System.out.println();
    	/*Test ausgelassen mit java nicht mÃ¶glich
    	System.out.println(P3.fixpoints());
    	System.out.println("Erwartet:List(3, 4, 8, 9)");
    	System.out.println();
    	*/
        System.out.println(p3);
        Permutation p5 = p3.pow(3);
        Permutation p6 = p3.pow(10000000);
        System.out.println("         "+p5+","+p6+","+p3.pow(-100000));
        System.out.println("Erwartet:(2,1,3,4,10,5,6,8,9,7),(1,2,3,4,5,6,7,8,9,10),(1,2,3,4,5,6,7,8,9,10)");
        System.out.println();

        System.out.println("         "+S10.createPermutation("(1 2)(3 4)(5 6 7 11)"));
        System.out.println("Erwartet:NaP");
        System.out.println();

        Permutation err1= PermutationImplementation.createPermutation(2,5,8,5,4,6,3);
        Permutation err2= PermutationImplementation.createPermutation(2,5,8,5,4,6,3,1);
        Permutation err3= PermutationImplementation.createPermutation(2,5,9,5,4,6,3,1);
        System.out.println("         "+err1+","+err2+","+err3);
        System.out.println("Erwartet:NaP,NaP,NaP");
        System.out.println();
    }

    public static void main(String[] args) {
        //Test Aufgabe2

        System.out.println("\n::: Test fuer Util :::");
        Aufgabe_1_1_Test.checkForMultipleNumberTest();
        Aufgabe_1_1_Test.calculateFacultyTest();

        System.out.println("\n::: Tests fuer Permutation :::");
        Aufgabe_1_1_Test.toStringTest();
        Aufgabe_1_1_Test.toCycleStringTest();
        Aufgabe_1_1_Test.getCycleTest();
        Aufgabe_1_1_Test.equalsTest();
        Aufgabe_1_1_Test.getInverseTest();
        Aufgabe_1_1_Test.getCompositionTest();
        Aufgabe_1_1_Test.testForAssociativity();
        Aufgabe_1_1_Test.getFixpointsTest();

        System.out.println("\n::: Tests fuer PermutationGroup :::");
        Aufgabe_1_1_Test.PermutationGroupConstruktorTest();

        //test der Aufgabe2
        Aufgabe_1_1_Test.aufgabe1_2();

        
//        PermutationGroup pg = PermutationGroupImplementation.createGroup(5);
//        Permutation p = pg.createPermutation(2,3,5,1,4);
//        System.out.println(p.toString());
        

        /*
        Permutation p = pg.createPermutation("(3 1 2)(5 4)");
        System.out.println(p);
        System.out.println(p.toCycleString());
        */
        
        //permutation anlegen und Rang berechnen
        
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Gruppe 2 - Tests für rank(permutation p) und rankToPerm(int rank)
        
        System.out.println("Tests Gruppe 3");
        
        PermutationGroup pg1 = PermutationGroupImplementation.createGroup(4);
        Permutation p1 = pg1.createPermutation(2,3,1,4);
        
        PermutationGroup pg2 = PermutationGroupImplementation.createGroup(4);
        Permutation p3 = pg2.createPermutation(1,2,3,4);
     
        
        System.out.println("         " + p1.toString());
        System.out.println("Erwartet:(2,3,1,4)");
        int a = pg1.rank(p1);
        System.out.println("          Rang = "+a); 
        System.out.println("Erwartet: Rang = 8");
        
        Permutation p2 = pg1.rankToPerm(14);
        System.out.println("         " + p2.toString());
        System.out.println("Erwartet:(3,2,1,4)");
        
        System.out.println("Anzahl Zyklen Test:");
        System.out.println("         " + p3.getNumberOfCycles());
        System.out.println("Erwartet:4");
        
  
       
        
        System.out.println("Gruppen-Id Test:");
        System.out.println("         " + pg1.getId());
        System.out.println("Erwartet:(1,2,3,4)");
        
     
        
    }

}
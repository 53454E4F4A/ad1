package aufgabe_1_1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 * 
 * @author Gruppe 3
 * @version 0.99
 */

public class Util {
    
    // Hilfsmethode um uebergebene ints auf doppelte Eintraege zu pruefen
    public static boolean checkForMultipleNumber(int[] numbers) {

        int start = 0;

        do {
            for (int i = start + 1; i < numbers.length; i++) {
                if (numbers[start] == numbers[i]) {
                    return true;
                }
            }

            start++;

        } while (start < numbers.length - 1);

        return false;
    }

    public static boolean checkForGaps(int[] numbers) {

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > numbers.length) {
                return true;
            }
        }

        return false;
    }

    public static int calculateFaculty(int val) {

        int fakultaet = 1;

        for (int zahl = 1; zahl <= val; zahl++) {
            fakultaet = fakultaet * zahl;
        }

        return fakultaet;
    }

    public static void exchangeInt(int[] arr, int index1, int index2) {

        int tmp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = tmp;
    }

    public static String parseExp(String exp) {

        char[] expStrings = {'⁰', '¹', '²', '³', '⁴', '⁵', '⁶', '⁷', '⁸', '⁹'};
        StringBuffer ret = new StringBuffer();
        if (exp.length() == 1 && exp.equals("1")) {
            return "";
        }
        for (int i = 0; i < exp.length(); i++) {
            ret.append(expStrings[Integer.parseInt(exp.substring(i, i+1))]);
        }
        return ret.toString();
    }
    
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Gruppe 2
    
    public static int calcRank(int... perm)
    {
    	//tmp: für die aktuellen Werte in orginal Reihenfolge
    	//index: für die Werte in sortierter Reihenfolge
    	if(perm.length>0)
    	{
        	
        	List<Integer> tmp = new ArrayList<Integer>();
        	List<Integer> index = new ArrayList<Integer>();
           
        	//perm bzw. Restwerte in die Listen packen
            for(int e: perm)
            {
            	tmp.add(e);
            	index.add(e);
            }
            int firstValue = tmp.get(0);
            
            //Index Liste Sortieren
            Collections.sort(index);
            
            //erstes Element entfernen
            tmp.remove(0);
            int [] nextCombi = new int [tmp.size()];
            
            for(int i = 0; i < nextCombi.length; i++)
            {
            	nextCombi[i] = tmp.get(i);
            }
            
            return index.indexOf(firstValue) * calculateFaculty(tmp.size()) + calcRank(nextCombi);
    		
    	}
    	else
    	{
            return 0;
    	}
    }
}

package aufgabe_1_1;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.HashSet;
import java.util.List;


/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 * 
 * @author Gruppe 3
 * @version 0.99
 */

class PermutationGroupImplementation implements PermutationGroup {

	private Permutation[] permArr;
	private int count;
	private final Permutation id;

	
	/**
	 * Wrapper fuer den PermutationGroup konstruktor wenn es sich nicht um eine PermutationGroup handelt wird NaS zurueckgegeben
	 * @param count
	 * @return
	 */
	public static PermutationGroup createGroup(int count){
		// Test ob eine korrekte Permutationsgruppen ID �bergeben wurde
		if (count < 1) {
			return nas;
		}
		return new PermutationGroupImplementation(count);
	}
	
	private PermutationGroupImplementation(int count) { // count = ID der
														// Permutationsgruppe
														// z.B. von der Gruppe
														// S3 ist 3 die ID
		this.count = count;
		
		int[] tmpint = new int[count];
		for(int i = 0;i<count;i++){
			tmpint[i] = i+1;
		}
		
		this.id = this.createPermutation(tmpint);

		int[] tmpVal; // enthaelt zu Beginn die Id, Werte werden fuer weitere
						// Permutationen getauscht.

			// Test ob eine Permutationsgruppen ID gr��er als 1 �bergeben wurde
		if (count == 1) {
			// Nein = Erstelle eine Permutationsgruppe mit der ID 1 und f�lle
			// Sie mit einer Permutation
			permArr = new PermutationImplementation[1];
			permArr[0] = PermutationImplementation.createPermutation(1);

		} else {
			// Ja = Erstelle ein Integer Array von der gr��e der ID der
			// Permutationsgruppe
			tmpVal = new int[count];

			// F�lle das Int Array mit Werten
			// Permutationsgruppen ID = 5 --> Int Array = 1, 2, 3, 4, 5
			for (int i = 0; i < tmpVal.length; i++) {
				tmpVal[i] = i + 1;
			}

			// Erstelle ein Permutations Array der gr��e N! wobei N die
			// Permutationsgruppen ID ist
			permArr = new PermutationImplementation[Util.calculateFaculty(count)];
		}
	}

	@Override
	public String toString() {
        return "S"+count;
    }

	/**
	 * erstellt eine Permutation
	 */
	@Override
	public Permutation createPermutation(int... zahlen) {
		HashSet<Integer> hs = new HashSet<Integer>();
		
		// Test: �bergabe > 0 ?
		if (zahlen.length == 0) {
			return Permutation.nap;
		}
		// Test: Doppelte Werte?
		for (int wert : zahlen) {
			if (!hs.add(wert)) {
				return Permutation.nap;
			}
		}
		// Test: Fehlende Werte
		if (hs.size() != count) {
			return Permutation.nap;
		}
		//Test: Zu gro�e Werte
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] > zahlen.length) {
				return Permutation.nap;
			}
		}
		return	PermutationImplementation.createPermutation(zahlen);
	}

    /**
	 * Erstellt eine Permutation aus einem zweidimensionalem Int-Array
	 *
	 * @param cycleList
	 * @return
	 */
	public Permutation createPermutation( int[][] cycleList) {
		int waaagh[] = new int[count];
		try{
			for (int i = 0; i < cycleList.length; i++) {
				for (int j = 0; j < cycleList[i].length; j++) {
					
					waaagh[cycleList[i][(j + cycleList[i].length - 1) % cycleList[i].length] -1 ] = cycleList[i][j];
				}
			}
		}catch(ArrayIndexOutOfBoundsException e){
			return Permutation.nap;
		}
		for (int grotz = 0; grotz < waaagh.length; grotz++) {
			if (waaagh[grotz] == 0) {
				waaagh[grotz] = grotz + 1;
			}
		}
		return PermutationImplementation.createPermutation(waaagh);
	}

    /**
     * erstelle eine Permutation aus einen String in Cyclic Notation
     *
     * @param s
     *            z.B. "(3 2)(1 4)"
     * @return
     */
    public Permutation createPermutation(String s) {
        if(s.equals(null) || s.equals("")){
        	return Permutation.nap;
        }
    	
    	try{
        	Permutation p = createPermutation(getArray(getCycle(s)));
            return p;
        }catch(Exception e){
            return Permutation.nap;
        }
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Gruppe 2
    public int rank(Permutation p)
    {
    	int [] tmp = p.getValues();
        
        return Util.calcRank(tmp);

    }

    public Permutation rankToPerm(int Rank)
    {
    	if(Rank < 0 || Rank > Util.calculateFaculty(count)-1)
    	{
    		 throw new IllegalArgumentException("Permutation Group: " + count + " only allows Rank's between:  " + 0 + " and " + (Util.calculateFaculty(count)-1));
    	}
    	
    	List<Integer> IdPerm = new ArrayList<Integer>();
    	int [] ready  = new int[this.count];
    	int n = this.count;
    	int index;
    	
    	for(int i = 0; i < this.count; i++)
    	{
    		IdPerm.add(i+1);
    	}
    	
    	for(int i = 0; i < this.count ; i++ )
    	{
    		index = Rank/Util.calculateFaculty(n-1);
    		Rank = Rank%Util.calculateFaculty(n-1);
    		
    		ready[i] = IdPerm.get(index);
    		
    		n--;
    		IdPerm.remove(index);
    	}
    	Permutation p = createPermutation(ready) ;
    	   	
    	return p;
	
    }
    
    /**
     * Zerlegt einen String f�r die cycleNotation in Substrings Substring =
     * (1,2,3)
     *
     * @param s
     * @return
     */
    private static ArrayList<String> getCycle(String s) {
        ArrayList<String> cycle = new ArrayList<String>();
        int head = 0;
        while (head < s.length()) {
            int tail = 0;
            while (tail + head < s.length() && s.charAt(tail + head) != ')') {
                tail++;
            }
            if (tail == 0) {
                return cycle;
            }
            cycle.add(s.substring(head, head + tail + 1));
            head += tail + 1;
        }
        return cycle;
    }
    
    /**
     * Wandelt einen String der Form (1,2,...42) in ein String um
     *
     * @param s
     * @return
     */
    private static int[] stringtointArray(String s) {
        int head = 0;
        int[] temp = new int[s.length()];
        int[] temptwo;
        int count = 0;

        while (s.charAt(head) != ')') {
            int tail = 1;// Finden den naechsten nummerischen Bereich
            while (s.charAt(head + tail) >= '0' && s.charAt(head + tail) <= '9') {
                tail++;
            }
            if (Arrays.asList(temp).contains(Integer.parseInt(s.substring(head + 1, head
                    + tail)))) {
                return null;
            }
            temp[count] = Integer.parseInt(s.substring(head + 1, head + tail));
            count++;
           // temp.add(Integer.parseInt(s.substring(head + 1, head + tail)));
            head += tail;
        }
        temptwo = new int[count];
        for(int i = 0; i < count; i ++) {
        	temptwo[i] = temp[i];
        }
        return temptwo;
    }

    public static int[][] getArray(ArrayList<String> cycle) {
        
		int[][] cycleArray = new int[cycle.size()][];
		for(int i = 0; i < cycle.size(); i++) {
			cycleArray[i] = stringtointArray(cycle.get(i));
		}
		
		return cycleArray;
}
    
    public Permutation getId(){
    	return id;
    }
}
package aufgabe_1_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 *
 * @author Gruppe 3
 * @version 0.99
 */

class PermutationImplementation implements Permutation {

    private int[] valArr;
    private Liste<Liste<Integer>> cyclenListe;
    private int numberOfCycles;

    
    
    /**
     * Wrapper Methode fuer den Construktor
     * bei einer ungueltigen Permutation wird nap zurueckgegeben
     * @param i
     * @return
     */
    public static Permutation createPermutation(int... i){
    	if(i.length == 0 || Util.checkForMultipleNumber(i) || Util.checkForGaps(i)){
    		return nap;
    	}
    	return new PermutationImplementation(i);
    }
    
    
    private PermutationImplementation(int... i) {
                valArr = new int[i.length];

                for (int j = 0; j < i.length; j++) {
                    valArr[j] = i[j];
                }
    }

    
    public int applyValue(int i) {

        // Ist i groesser als die Permutation lang ist ?
        if (i > valArr.length) {
            // Ja
        	throw new IllegalArgumentException();
        }

        // Nein
        return valArr[i - 1];
    }

    public Liste<Integer> cycle(int val) throws IndexOutOfBoundsException
    {
        this.toCycleString();
        return cyclenListe.get(val);

    }

    /**
     * bestimmt das Inverse der Permutation
     */
    public Permutation inverse() {

        int[] arrRes = new int[valArr.length];
        int tmp;

        // Bilden der Inversen Permutation
        // (1,2,3) (3,1,2) (1,2,3)
        //
        // (3,1,2) (1,2,3) (2,3,1)
        for (int i = 0; i < valArr.length; i++) {
            tmp = valArr[i];
            arrRes[tmp - 1] = i + 1;
        }

        return new PermutationImplementation(arrRes);
    }

    /**
     * bestimmt die Komposition zweier Permutation
     */
    public Permutation compose(Permutation p) {

        if (p.getClass() == nap.getClass()){
            System.out.println("getComposition(): NaP!");

            return nap;
        }

        int[] arrP1 = valArr;
        int[] arrP2 = ((PermutationImplementation) p).getValues();
        int[] arrRes = new int[((PermutationImplementation) p).getValues().length];
        int tmp;

        // Hat die uebergebene Permutation dieselbe Laenge?
        if (arrP1.length == arrP2.length) {
            // Ja

            // p2(3,5,1,4,2,) o p1(2,4,5,1,3) -> (5,4,2,3,1)
            for (int i = 0; i < arrP1.length; i++) {
                tmp = arrP2[i];
                arrRes[i] = arrP1[tmp - 1];
            }

            return new PermutationImplementation(arrRes);

        } else {
            return nap;
        }
    }

//    @Override
//    public boolean equals(Object o) {
//        // Ist das uebergebene Object vom Typ Permutation?
//        if (o instanceof Permutation) {
//            return this.equals((Permutation)o);
//        } else {
//            return false;
//        }
//
//    }



    public boolean equals(Permutation p) {
        int[] tmp;

        tmp = ((PermutationImplementation) p).getValues();

        // Hat die uebergebene Permutation die selbe Laenge?
        if ((valArr.length == tmp.length) && p instanceof Permutation) {
            // Ja

            // Sind alle Elemente gleich?
            for (int i = 0; i < valArr.length; i++) {
                if (valArr[i] != tmp[i]) {
                    // Nein
                    return false;
                }
            }

            // Ja
            return true;
        } else {
            return false;
        }
        }

    
    /**
     * bestimmt die Fixpoints einer Permutation
     */
    public List<Integer> fixpoints() {

        // Wenn ein Wert aus der Permutation mit dem
        // gegenueberliegendem Wert aus der Id uebereinstimmt,
        // ist es ein Fixpoint.

        // (1,2,3,4,5)
        // ( | )
        // (3,5,1,4,2)

        Liste<Integer> tmp = new Liste<Integer>();
        for (int i = 0; i < valArr.length; i++) {
        	
            if (valArr[i] == i + 1) {
                tmp.add(valArr[i]);
            }
        }
        return tmp;
    }

    /**
     * Diese Methode gibt die Anzahl der Fixpoints zurueck
     */
   public int numFixpoints() {

        int count = 0;

        for (int i = 0; i < valArr.length; i++) {
            if (valArr[i] == i + 1) {
                count++;
            }
        }

        return count;
    }

    public int[] getValues() {
        
    	return valArr;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder("(");

        for (int i = 0; i < valArr.length; i++) {
            sb.append(valArr[i]);

            if ((i >= 0) && (i < valArr.length - 1)) {
                sb.append(",");
            }
        }

        sb.append(")");

        return sb.toString();
    }

    public String toCycleString() {

        this.cyclenListe = new Liste<Liste<Integer>>();

        int value; // Variable, um index-richtig einen Zyklus abzulaufen
        int cycleIndex = 0; // Index, an dem das Vorkommen einer Variablen eines
        // Zyklus im cycleBuffer Array abgelegt wird

        boolean cycle; // Flag, ob gerade ein Zyklus abgearbeitet wurde
        int[] cycleBuffer = new int[valArr.length]; // Puffer fuer die
        // Variablen, die in einem
        // Zyklus vorkommen
        boolean[] visited = new boolean[valArr.length]; // Array zum Pruefen,
        // welche Zahlen bereits
        // abgearbeitet wurden

        int counter = 0; // Zaehler fuer die Summe der Zyklus-Kandidaten
        boolean unsorted; // Flag, um Zyklus-Array im Bubble-Sort zu sortieren
        int temp; // Temp-Variable fuer Bubble-Sort

        StringBuilder sb = new StringBuilder(); // StringBuilder fuer den
        // Ouput-String

        // START
        // Anfangen die Permutation zu durchlaufen
        for (int i = 0; i < valArr.length; i++) {

            // Temporäre Liste für ein Cycle
            Liste<Integer> tmpCycle = new Liste<Integer>();

            // Wenn der Wert in Index i noch nicht geprueft wurde
            if (!visited[i]) {

                cycle = false; // Zyklus-Flag zuruecksetzen
                sb.append('('); // Klammern oeffnen fuer naechsten Zyklus /
                numberOfCycles++;
                // Fixpunkt
                cycleBuffer[cycleIndex] = valArr[i]; // Wert i der Permutation
                // wird im cycleBuffer
                // abgelegt
                counter++; // Zyklus-Kandidat gefunden
                visited[i] = true; // Wert in i als abgearbeitet markieren

                // value dekrementieren, so ist es index-richtig.
                // Dadurch kann ein Zyklus anhand des Wertes durchlaufen werden.
                // Die Arrays starten bei Index 0, die Permutationen bei 1!
                value = (valArr[i] - 1);

                // Falls der (index-richtige) Wert noch nicht geprueft wurde,
                // wird der Zyklus nun Wert fuer Wert durchlaufen
                while (!(visited[value])) {

                    cycleIndex++; // Im cycleBuffer zum naechsten Index zum
                    // Speichern vorruecken
                    cycleBuffer[cycleIndex] = valArr[value]; // Wert des Zyklus
                    // im
                    // cycleBuffer
                    // ablegen
                    counter++; // Zyklus-Kandidat gefunden
                    visited[value] = true; // Wert in visited als abgearbeitet
                    // markieren
                    value = (valArr[value] - 1); // value index-richtig setzen
                    cycle = true; // Zyklus-Flag setzen
                } // while

                // Wenn ein Zyklus gefunden wurde, die Zyklus-Kandidaten korrekt
                // sortierten
                if (cycle) {

                    // Bubble-Sort Flag setzen und Array fuer Zyklus-Kandidaten
                    // anlegen
                    unsorted = true;
                    int[] cycleCandidates = new int[counter];

                    // Gueltige Zyklus-Variablen (groesser 0) aus cycleBuffer
                    // nach cycleCandidates uebertragen
                    for (int j = 0; j < cycleCandidates.length; j++) {
                        for (int k = 0; k < cycleBuffer.length; k++) {
                            if (cycleBuffer[k] > 0) {
                                cycleCandidates[j] = cycleBuffer[k];
                                j++;
                            } // if
                        } // for
                    } // for

                    // cycleCandidates mit Bubble-Sort aufsteigend sortieren
                    while (unsorted) {

                        unsorted = false; // Bubble-Sort Flag zuruecksetzen

                        for (int k = 0; k < cycleCandidates.length - 1; k++) {
                            if (cycleCandidates[k] > cycleCandidates[k + 1]) {

                                temp = cycleCandidates[k];
                                cycleCandidates[k] = cycleCandidates[k + 1];
                                cycleCandidates[k + 1] = temp;
                                unsorted = true; // Bubble-Sort Flag setzen
                            } // if
                        } // for
                    } // while

                    // Gefundene und sortierte cycleCandidates im StringBuilder
                    // ablegen
                    for (int j = 0; j < cycleCandidates.length; j++) {
                        sb.append(cycleCandidates[j]);
                        sb.append(" ");

                        // Cyclen paare in die tmp-Liste absichern
                        tmpCycle.add(cycleCandidates[j]);
                    } // for

                    // Wenn ein Leerzeichen am Ende eines Zyklus vorhanden ist,
                    // dieses entfernen
                    if (sb.charAt(sb.length() - 1) == ' ') {
                        sb.deleteCharAt(sb.length() - 1);
                    } // if

                } // if - ENDE der Zyklus-Verarbeitung

                // Wenn KEIN Zyklus abgearbeitet wurde, gefundenen Fixpunkt im
                // StringBuilder ablegen
                if (!cycle) {
                    sb.append(valArr[i]);

                    // Fixpunkte in die tmp-Liste sichern.
                    tmpCycle.add(valArr[i]);
                } // if

                // Operation an Zyklus, bzw. Fixpunkt mit Klammer beenden,
                // counter und cycleBuffer leeren
                sb.append(')');
                counter = 0;
                cycleBuffer = new int[valArr.length];
            } // if

            // Wenn die tmp-Liste nicht leer ist, dann wird sie in der
            // CycleListe abgesichert.
            if (tmpCycle.size() > 0) {
                cyclenListe.add(tmpCycle);
            }

        } // for

        return sb.toString();
    }

    public Map<Integer, Integer> cycleType() {

        // erstelle alle cycles
        this.toCycleString();
        //TreeMap erstellen, Key als Basis und Value fuer den Exponent
        Map<Integer, Integer> retMap = new TypeMap<Integer, Integer>();
        int tmpIndex = 0;

        for (int i = 0; i < this.cyclenListe.size(); i++) {
            // hole cycle länge und sicher sie in tmpIndex ab
            tmpIndex = cyclenListe.get(i).size();

            //Wenn die Cycle Laenge vorhanden ist, dann....
            if (retMap.containsKey(tmpIndex)) {
                //erhoehe Value um 1 (Exponent)
                retMap.put(tmpIndex, (retMap.get(tmpIndex)) + 1);
            } else {
                //Fuege neue Key (Basis) hinzu
                retMap.put(tmpIndex, 1);
            }

        }

        return retMap;

    }


    /**
     * berrechnet die order einer Permutation
     */
    public int order(){
        this.toCycleString();
        Liste<Liste<Integer>> list = this.cyclenListe;
        Liste<Integer> temp = new Liste<Integer>();
        for(int i = 0; i < list.size(); i++){
            temp.add(list.get(i).size());
        }
        return calckgv(temp);
    }


    /**
     * berrechnet das kleinste gemeinsame vielfache des Arrays
     * @param numbers
     * @return
     */
    private int calckgv(ArrayList<Integer> numbers) {
        if(numbers == null){
            throw new NullPointerException();
        }

        int kgv = 1;
        int exp = 0;
        int n = 2;
        int count = 0;

        while(numbers.size() > 0){
            for(int i = 0; i < numbers.size(); i++){
                while(numbers.get(i) % n == 0){
                    numbers.set(i, numbers.get(i) / n);
                    count++;
                }
                if(count > exp){
                    exp = count;
                }
                count = 0;
                if(numbers.get(i) < 2){
                    numbers.remove(i);
                    i--;
                }
            }
            kgv *= Math.pow(n, exp);
            n++;
            exp = 0;
        }
        return kgv;
    }
   
    public int getNumberOfCycles(){
    	this.toCycleString();
    	return numberOfCycles;
    }

    public Permutation pow(int exp){
        if(exp == 0){
            return nap;
        }
        int order = order();
        Permutation p = new PermutationImplementation(this.valArr);
        exp = (order + ((exp - 1) % order)) % order;
        for(int i = 0; i < exp; i++){
            p = p.compose(this);
        }
        return p;
    }
    


}

/** 
 * ArrayList mit redefinierter ToString Methoden
 */

class Liste<E> extends ArrayList<E> {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public String toString() {

        StringBuilder ret = new StringBuilder();

        ret.append("(");

        for (E index : this) {
            ret.append(index + " ");
        }

        if(ret.length() > 1) ret.deleteCharAt(ret.length() - 1);

        return ret.append(")").toString();

    }
}



/** 
 * TypeMap mit redefinierter ToString Methoden
 */
class TypeMap<K, V> extends TreeMap<K, V> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public String toString() {

        StringBuilder ret = new StringBuilder();

        ret.append("[");

        for (int i = 1; i <= this.size(); i++) {

            ret.append(this.keySet().toArray(new Integer[this.size()])[i - 1].toString()
                    + Util.parseExp(this.get(this.keySet().toArray(new Integer[this.size()])[i - 1]).toString())
                    + " ");
        }

        ret.deleteCharAt(ret.length() - 1);

        return ret.append("]").toString();

    }
}
package aufgabe_1_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 *
 * @author Gruppe 3
 * @version 0.99
 */

interface Permutation {

    public static final Permutation nap = NaP.getNaP();
    

    /**
     * Ueberprueft den Wertebereich
     * und liefert valArr[i - 1] zurŸck
     */
    public int applyValue(int i); 
     
    /**
     * Liste mit den Cycles
     */
    public List<Integer> cycle(int i);
    
    /**
     * Liste mit den Fixpunkten
     */
    public List<Integer> fixpoints(); 
    
    /**
     * bestimmt die Komposition zweier Permutation
     */
    public Permutation compose(Permutation p);
    
    /**
     * bestimmt das Inverse der Permutation
     */
    public Permutation inverse(); 
      
    /**
     * Vergleicht zwei Permutation miteinander auf Gleichheit
     * 
     */
    public boolean equals(Permutation p); 

    /**
     * Ausgabe des Strings
     */
    public String toString(); 


    /**
     * Ausgabe der CycleStrings
     */
    public String toCycleString();

    
    /**
     * Diese Methode gibt die Anzahl der Fixpoints zurueck
     */
    public int numFixpoints();

    public Map<Integer, Integer> cycleType();

    /**
     * berrechnet die Order einer Permutation
     */
    public int order();

    /**
     * berrechnet die Potenz
     */
    public Permutation pow(int exp);

    /**
     * Diese Methode gibt die Zahlen der Permutation in einem Array zurück
     */
    public int[] getValues(); 
    
    public int getNumberOfCycles();
    
    class NaP implements Permutation {
    	private static NaP nap = new NaP();
    	
    	private NaP(){
    	}
    	
    	public static NaP getNaP(){
    		return nap;
    	}
    	
    	@Override
    	public String toString() {
    		return "NaP";
    	}

    	@Override
    	public int applyValue(int i) {
    		return 0;
    	}

    	@Override
    	public ArrayList<Integer> cycle(int i) {
    		return null;
    	}

    	@Override
    	public List<Integer> fixpoints() {
    		return null;
    	}

    	@Override
    	public Permutation compose(Permutation p) {
    		return nap;
    	}

    	@Override
    	public Permutation inverse() {
    		return nap;
    	}

    	@Override
    	public String toCycleString() {
    		return "NaP";
    	}

    	@Override
    	public int numFixpoints() {
    		return 0;
    	}

    	@Override
    	public TypeMap<Integer, Integer> cycleType() {
    		return null;
    	}

    	@Override
    	public int order() {
    		return 0;
    	}

    	@Override
    	public Permutation pow(int exp) {
    		return nap;
    	}

    	@Override
    	public boolean equals(Object obj){
    		return (obj.getClass() == getClass());
    	}

        @Override
        public boolean equals(Permutation p) {
            return (p.getClass() == getClass());
        }

		@Override
		public int[] getValues() {
			
			return null;
		}
		
		public int getNumberOfCycles(){
			return 0;
		}

    }
    
}

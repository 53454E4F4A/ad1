package aufgabe_1_1;



/**
 * Wenn ihr was an dem Programm aendert, vergesst nicht die Versionsnummer
 * irgendwie zu aendern.
 * 
 * @author Gruppe 3
 * @version 1.00
 */

interface PermutationGroup {
	public static final PermutationGroup nas = NaS.getNaS();

    public String toString();
    
    public Permutation createPermutation(int... i);

    public Permutation createPermutation(int[][] cycleList);


    /**
     * erstelle eine Permutation aus einen String in Cyclic Notation
     *
     * @param s
     *            z.B. "(3 2)(1 4)"
     * @return
     */
    public Permutation createPermutation(String s);
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //Gruppe 2
    
    /**
     * Gibt den Rank einer Permutation zurück.
     * (2,3,1,4) = Rang 8 
     */
    int rank(Permutation p);
    
    /**
     * Erzeugt eine Permutation anhand eines Rangs
     * Gruppe 4 - Rang 14 --> Permutation(3,2,1,4)
     * 
     */
    Permutation rankToPerm(int Rank);
    
    Permutation getId();
    
    
    class NaS implements PermutationGroup{
    	private static PermutationGroup nas = new NaS();
    	
    	public static PermutationGroup getNaS(){
    		return nas;
    	}
    	
    	private NaS(){
    	}

    	@Override
    	public String toString(){
    		return "NaS";
    	}

		@Override
		public Permutation createPermutation(int... i) {

			return Permutation.nap;
		}

		@Override
		public Permutation createPermutation(int[][] cycleList) {
			return Permutation.nap;
		}

        @Override
        public Permutation createPermutation(String s) {
            return Permutation.nap;
        }
        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Gruppe 2
        public int rank(Permutation p)
        {
        	return 1;
        }
        public Permutation rankToPerm(int Rank)
        {
        	return Permutation.nap;
        }
        
        public Permutation getId(){
        	return Permutation.nap;
        }
    	
    }
}
